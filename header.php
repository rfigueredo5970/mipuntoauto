<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="Web Cerrajería Mi Punto Auto">
	<meta name="keywords" content="Cerrajería">
	<meta name="author" content="Rafael Figueredo, Pierangela Martinez">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- pagetitle -->
	<title><?php wp_title(' | ', 'echo', 'right'); ?><?php bloginfo('name'); ?> </title>
	
	<!-- stylesheets -->
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url')?>/css/normalize.css">
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url')?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url')?>/fonts/icomoon/style.css">
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('stylesheet_url')?>">
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url')?>/css/responsive.css">


	<!-- js -->
	<script type="text/javascript" src="<?php bloginfo('template_url')?>/js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url')?>/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url')?>/js/smoothscrolling.jquery.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url')?>/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>
	<script type="text/javascript" src="<?php bloginfo('template_url')?>/js/script.js"></script>
</head>
<body>

	<div id="inicio"></div>

	<header>
		<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">    	
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>   

      <a class="navbar-brand" href="<?php bloginfo('url'); ?>">
      	<img src="<?php bloginfo('template_url')?>/img/logo.png"  max-heigth="120px" alt="logo#1" class="logo img-responsive">
      	<img src="<?php bloginfo('template_url')?>/img/logo-name.png"  max-heigth="120px" alt="Cerrajería Mi Punto Auto" class="logo img-responsive">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <?php /* Primary navigation */
            wp_nav_menu( array(
              'menu' => 'top_menu',
              'theme_location' => 'top_menu',
              'depth' => 2,
              'container' => false,
              'menu_class' => 'nav navbar-nav navbar-right menu',
              //Process nav menu using our custom nav walker
              'walker' => new wp_bootstrap_navwalker())
            );
      ?>                 
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
	<?php wp_head(); ?> 
	</header>	

	