<?php

add_action( 'customize_register', 'mipuntoauto_customize_register' );

function mipuntoauto_customize_register( $wp_customize ) {

	// Bienvenidos
	$wp_customize->add_section('mipuntoauto_bienvenidos',array(
		'title'=>"Bienvenidos"
	));

	$wp_customize->add_setting('mipuntoauto_bienvenidos_maintitle',array(
		'default'=>'Bienvenidos'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'mipuntoauto_bienvenidos_maintitle_control',array( 
		'label'=> 'Saludo de Bienvenida',
		'section' => 'mipuntoauto_bienvenidos',
		'settings' => 'mipuntoauto_bienvenidos_maintitle'
	)));

	$wp_customize->add_setting('mipuntoauto_bienvenidos_secondarytitle',array(
		'default'=>'Seguridad al alcance de todos'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'mipuntoauto_bienvenidos_secondarytitle_control',array( 
		'label'=> 'Titulo Explicativo (Relevante para SEO)',
		'section' => 'mipuntoauto_bienvenidos',
		'settings' => 'mipuntoauto_bienvenidos_secondarytitle'
	)));

	$wp_customize->add_setting('mipuntoauto_bienvenidos_imagen');

	$wp_customize->add_control(new WP_Customize_Cropped_Image_Control($wp_customize, 'mipuntoauto_bienvenidos_imagen_control',array( 
		'label'=> 'Banner de Bienvenida',
		'section' => 'mipuntoauto_bienvenidos',
		'settings' => 'mipuntoauto_bienvenidos_imagen',
		'width' => 797,
		'height' => 423
	)));

	// Acerca de Nosotros
	
	$wp_customize->add_section('mipuntoauto_nosotros',array(
		'title'=>"Quienes somos"
	));

	$wp_customize->add_setting('mipuntoauto_nosotros_text',array(
		'default'=>"nim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS."
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'mipuntoauto_nosotros_text_control',array( 
		'label'=> '¿Quienes somos?',
		'section' => 'mipuntoauto_nosotros',
		'settings' => 'mipuntoauto_nosotros_text',
		'type'=>'textarea'
	)));

	$wp_customize->add_setting('mipuntoauto_nosotros_imagen');

	$wp_customize->add_control(new WP_Customize_Cropped_Image_Control($wp_customize, 'mipuntoauto_nosotros_imagen_control',array( 
		'label'=> 'Imagen de Quienes Somos',
		'section' => 'mipuntoauto_nosotros',
		'settings' => 'mipuntoauto_nosotros_imagen',
		'width' => 672,
		'height' => 475
	)));

	// Mision y vision
	
	$wp_customize->add_section('mipuntoauto_mision',array(
		'title'=>"Mision y Vision"
	));

	$wp_customize->add_setting('mipuntoauto_mision_text',array(
		'default'=>"shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of "
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'mipuntoauto_mision_text_control',array( 
		'label'=> '¿Quienes somos?',
		'section' => 'mipuntoauto_mision',
		'settings' => 'mipuntoauto_mision_text',
		'type'=>'textarea'
	)));

	// Footer Contacto
	
	$wp_customize->add_section('mipuntoauto_contacto',array(
		'title'=>"Datos De Contacto"
	));

	$wp_customize->add_setting('mipuntoauto_contacto_text',array(
		'default'=>"Cerrajería Mi Punto Auto. Rif: 0000000. <br>
						Dirección: Estado Vargas, Venezuela<br>
						tlf: 02120000000 / 04140000000 "
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'mipuntoauto_contacto_text_control',array( 
		'label'=> 'Datos Contacto',
		'section' => 'mipuntoauto_contacto',
		'settings' => 'mipuntoauto_contacto_text',
		'type'=>'textarea'
	)));

}
