<?php

include('customizer.php');

	
@ini_set( 'upload_max_size' , '20M' );
@ini_set( 'post_max_size', '20M');
@ini_set( 'max_execution_time', '300' );
	
	// Register Custom Navigation Walker
	require_once get_template_directory() . '/wp-bootstrap-navwalker.php';


	register_nav_menus( array(
        'top_menu' => __( 'Top Menu', 'mipuntoauto' ),
        'footer_nav' => __( 'Footer Menu', 'mipuntoauto' ),
        'social_bar' =>__( 'Social Bar', 'mipuntoauto' ),
        'xs_social_bar' =>__( 'XS Social Bar', 'mipuntoauto' )
	) );



	add_theme_support('post-thumbnails');
	add_image_size( 'list_articles_thumbs', 200, 200, true );