	<footer>
	
		<div class="container caja1">
			<div class="row">
				<article class="col-sm-12">
					<?php echo wpautop(get_theme_mod('mipuntoauto_contacto_text'));?>
				</article>			
			</div>
		</div>
			<div class="clearfix"></div>
			<div class="container-fluid menu-footer-container">
				<nav class="menu-footer">
				<?php wp_nav_menu( array( 'theme_location' => 'footer_nav', 'container' => false,
              'menu_class' => 'list-inline foot', ) );?>
				</nav>
				<p class="small"> <i>Desarrollo Web por Rafael Figueredo | Diseño Web por Pierangela Martínez | Diseño de Imagen Corporativa  por Enyerbert Gonzalez</i></p>
			</div>		
	</footer>
	
<?php wp_footer(); ?>
</body>
</html>