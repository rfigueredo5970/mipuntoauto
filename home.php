<?php 
	get_header();

	//response generation function
	$response = ""; 
	if(isset($_POST['name'])){
		$response = $_POST['name'];		
		$response = "OK";

	}

if(isset($_POST['submit'])){

	//response generation function
	  $response = "OK";
	 
	  //function to generate response
	  function my_contact_form_generate_response($type, $message){
	 
	    global $response;
	 
	    if($type == "success") $response = "<div class='success'>{$message}</div>";
	    else $response = "<div class='error'>{$message}</div>";
	 
	  }
	
	  //response messages
	$missing_content = "Por favor rellenar todos los campos";
	$email_invalid   = "La dirección de correo es inválida";
	$message_unsent  = "El mensaje no pudo ser enviado. Intente de nuevo.";
	$message_sent    = "¡Gracias! Su mensaje ha sido enviado.";
	 
	//user posted variables
	$name = $_POST['name'];
	$email = $_POST['email'];
	$message = $_POST['message'];
	$city = $_POST['city'];
	 
	//php mailer variables
	$to = get_option('admin_email');
	$subject = "Someone sent a message from " . get_bloginfo('name');
	$headers = 'From: ' . $email . "\r\n" .
	  'Reply-To: ' . $email . "\r\n";
	
	 if ($_POST['submitted']!=1){
	
		 my_contact_form_generate_response("error", $missing_content);
	 
	 }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {	
	//validate email
		my_contact_form_generate_response("error", $email_invalid);
	
	 }elseif (empty($name) || empty($message)) {
	 //email is valid
	 //validate presence of name and message	
	
		my_contact_form_generate_response("error", $missing_content);
	
	 }else{
	 	//send email
	 	my_contact_form_generate_response("success", $message_sent); //message sent!
	 }
}
?>	
	<section class="intro">
		<div class="container">
			<div class="row">
				<article class="col-xs-12 text-center">
					<h2><?php echo get_theme_mod('mipuntoauto_bienvenidos_maintitle');?></h2>
					<img src="<?php if (get_theme_mod( 'mipuntoauto_bienvenidos_imagen' )) : echo wp_get_attachment_url(get_theme_mod('mipuntoauto_bienvenidos_imagen') ); else: echo bloginfo('template_url').'/img/welcome.gif'; endif; ?>" alt="banner Bienvenida" class="intro_banner img-responsive center-block">
					<h1><?php echo get_theme_mod('mipuntoauto_bienvenidos_secondarytitle');?></h1>
				</article>

			</div>
		</div>
	</section>
<div id="nosotros" ></div>
<section class="content-nosotros">
	<div class="container">
		<div class="row">
			 <article class="col-xs-12 center-block text-center">
			 	<img src="<?php if (get_theme_mod( 'mipuntoauto_nosotros_imagen' )) : echo wp_get_attachment_url(get_theme_mod('mipuntoauto_nosotros_imagen') ); else: echo bloginfo('template_url').'/img/door-lock.jpg'; endif; ?>" alt="imagen: Quienes Somos" class="thumb img-responsive">
			 	<br>
			 			<?php echo wpautop(get_theme_mod('mipuntoauto_nosotros_text'));?> 
			 	<article class="col-xs-12 center-block text-center">		
			 		<img src="<?php bloginfo('template_url')?>/img/nosotros.svg" alt="" class="nosotros_banner img-responsive center-block">
			 	</article>
			 </article>	
		</div>

	</div>
</section>
<section class="content-productos">
<div id="productos" ></div>
	<div class="container">
	<h2>Productos</h2>

	<?php $productos_query = new WP_Query( array( 'category_name' => 'productos', 'posts_per_page' => 3 ) ) ;?>

		<div class="row">
			<?php if ( $productos_query->have_posts() ) : while ( $productos_query->have_posts() ) : $productos_query->the_post(); 
					$post = get_post();
					if(($productos_query->current_post + 1) == $productos_query->post_count) :
				?>
			 <article class="col-xs-12 col-sm-6 col-md-4 hidden-xs hidden-sm center-block text-center">
			 	<h3><?php the_title();?></h3>
			 	<br>
				<?php
					if ( $productos_query->has_post_thumbnail() ) {
						
							$productos_query->the_post_thumbnail('list_articles_thumbs', array('class' => 'thumb img-responsive img-circle'));

					}
					else{
						echo '<img src="' . get_bloginfo( 'template_url' ) . '/img/lock200x200.png" alt="" class="thumb img-responsive img-circle">';
					}
				?>
			 	<p><?php the_content(); ?></p>
			 </article>
			<?php else: ?>
				<article class="col-xs-12 col-sm-6 col-md-4 center-block text-center">
			 	<h3><?php the_title(); ?></h3>
			 	<br>
				<?php
					if ( has_post_thumbnail() ) {
										
							the_post_thumbnail('list_articles_thumbs', array('class' => 'thumb img-responsive img-circle'));
					}
					else{
													
					echo '<img src="' . get_bloginfo( 'template_url' ) . '/img/lock200x200.png" alt="" class="thumb img-responsive img-circle">';
					}
				?>
			 	<p><?php the_content(); ?></p>
			 </article>

			 <?php endif; endwhile; else: ?>		
				<h4>No se encontraron productos</h4>
			<?php endif; ?>
	<?php  wp_reset_query() ?>

		</div>
		<div class="row">
			 <article class="col-xs-4 col-xs-offset-4 col-sm-6 col-md-4 col-sm-offset-8">
			 		<a class="link-more" href="<?php  echo home_url()?>/productos">Ver Más Productos</a>
			 </article>
		</div>
		
	</div>
</section>
<section class="content-servicios">
<div id="servicios"></div>
	<div class="container">
	<h2>Servicios</h2>

	<?php $servicios_query = new WP_Query( array( 'category_name' => 'servicios', 'posts_per_page' => 3 ) ) ;?>

		<div class="row">
			<?php if ( $servicios_query->have_posts() ) : while ( $servicios_query->have_posts() ) : $servicios_query->the_post(); 
					$post = get_post();
					if(($servicios_query->current_post + 1) == $servicios_query->post_count) :
				?>
			 <article class="col-xs-12 col-sm-6 col-md-4 hidden-xs hidden-sm center-block text-center">
			 	<h3><?php the_title();?></h3>
			 	<br>
				<?php
					if ( $servicios_query->has_post_thumbnail() ) {
						
							$servicios_query->the_post_thumbnail('list_articles_thumbs', array('class' => 'thumb img-responsive img-circle'));

					}
					else{
						echo '<img src="' . get_bloginfo( 'template_url' ) . '/img/lock200x200.png" alt="" class="thumb img-responsive img-circle">';
					}
				?>
			 	<p><?php the_content(); ?></p>
			 </article>
			<?php else: ?>
				<article class="col-xs-12 col-sm-6 col-md-4 center-block text-center">
			 	<h3><?php the_title(); ?></h3>
			 	<br>
				<?php
					if ( has_post_thumbnail() ) {
										
							the_post_thumbnail('list_articles_thumbs', array('class' => 'thumb img-responsive img-circle'));
					}
					else{
													
					echo '<img src="' . get_bloginfo( 'template_url' ) . '/img/lock200x200.png" alt="" class="thumb img-responsive img-circle">';
					}
				?>
			 	<p><?php the_content(); ?></p>
			 </article>

			 <?php endif; endwhile; else: ?>		
				<h4>No se encontraron servicios</h4>
			<?php endif; ?>
	<?php  wp_reset_query() ?>

		</div>
		<div class="row">
			 <article class="col-xs-4 col-xs-offset-4 col-sm-6 col-md-4 col-sm-offset-8">
			 		<a class="link-more" href="<?php  echo home_url()?>/servicios">Ver Más Servicios</a>
			 </article>
		</div>	
	</div>
</section>

<section class="content-mision">
<div id="mision" ></div>
	<div class="container">
		<div class="row">
			 <article class="col-sm-12 center-block text-center">
			 	<br>
			 		<?php echo wpautop(get_theme_mod('mipuntoauto_mision_text'));?>
				<div class="col-xs-12 center-block">		
			 			<img src="<?php bloginfo('template_url')?>/img/mision.svg" alt="" class="mision_banner img-responsive center-block">
			 	</div>			 	
			 </article>	
		</div>
	</div>
</section>
<section class ="content-contacto">
<div id="contacto" ></div>
<div class="container">
	<h2>Contacto</h2>
	<br>
	<div class="row">
		<article class="col-xs-12 col-sm-6 col-lg-7">
					<!-- <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps?f=q&source=s_q&hl=en&geocode=&q=15+Springfield+Way,+Hythe,+CT21+5SH&aq=t&sll=52.8382,-2.327815&sspn=8.047465,13.666992&ie=UTF8&hq=&hnear=15+Springfield+Way,+Hythe+CT21+5SH,+United+Kingdom&t=m&z=14&ll=51.077429,1.121722&output=embed"></iframe> -->
					<?php echo do_shortcode( '[wpgmza id="1"]' ); ?>
		</article>
		<article class="col-xs-12 col-sm-6 col-lg-5 form-box">
			<div class="panel">
				<div class="panel-body">
					<?php echo do_shortcode( '[contact-form-7 id="78" title="Contact form 1" ]' ); ?>	
			</div>
		</div>
		</article>
	</div>
	
</div>
</section>

<!-- Sidebar -->
<?php
	get_sidebar();
?>

<!-- Footer -->
<?php
	get_footer();
?>
