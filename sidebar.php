<aside>
	<!-- Barra social -->
	<div class="social hidden-xs">
		<!-- <ul>
			<li><a href=""><span>Facebok</span><div class="icono icon-facebook"></div></a></li>
			<li><a href=""><span>Twitter</span><div class="icono icon-twitter"></div></a></li>
			<li><a href=""><span>Instagram</span><div class="icono icon-instagram"></div></a></li>
			<li><a href=""><span>Youtube</span><div class="icono icon-youtube"></div></a></li>
		</ul> -->
		<?php wp_nav_menu( array( 'theme_location' => 'social_bar', 'container' => false ) );?>
	</div>

		<div class="xs-social visible-xs">
	<!-- 	<ul>
			<li><a href=""><div class="icono icon-facebook"></div></a></li>
			<li><a href=""><div class="icono icon-twitter"></div></a></li>
			<li><a href=""><div class="icono icon-instagram"></div></a></li>
			<li><a href=""><div class="icono icon-youtube"></div></a></li>
		</ul> -->
		<?php wp_nav_menu( array( 'theme_location' => 'xs_social_bar', 'container' => false ) );?> 
	</div>

	<!-- Scroll up button -->
	<div class="menu">
			<a href="#inicio" class="scrollup"></a>
	</div>
</aside>