$(function(){ 

			 $(".menu a").smoothscrolling({ 
   					 offsetTop: 100 // cantidad de pixeles que tiene el elemento flotante
 				});

			 $(".foot a").smoothscrolling({
			 	 offsetTop: 100 // cantidad de pixeles que tiene el elemento flotante
			 });

			 $(window).scroll(function(){
			 	if($(this).scrollTop() > 100){

			 		$('.scrollup').fadeIn();

			 	}
			 	else{

			 		$('.scrollup').fadeOut();
			 	}
			 });

			 // Validaciones del formulario de contacto


			$("#commentForm").validate({

			 	rules: {

    				name: {
      					required: true,
      					minlength: 3
    				},
    				
    				email: {
      					required: true,
      					email: true
    				},

    				city: {
      					required: true,
      					minlength: 3
    				},

    				message: {
      					required: true,
      					minlength: 40
    				}
  				},

  				messages: {

    				name: {
      					required: "Por favor especifíca tu nombre",
      					minlength: jQuery.validator.format("Debe tener mínimo {0} caracteres")
    					},

    				city: {
      					required: "Por favor especifíca tu ciudad",
      					minlength: jQuery.validator.format("Debe tener mínimo {0} caracteres")
    					},

    				message: {
      					required: "Por favor introduce tu mensaje",
      					minlength: jQuery.validator.format("Debe tener mínimo {0} caracteres")
    					},

    				email: {
      					required: "Necesitamos tu correo electrónico para contactarte",
      					email: "Debe tener el formato name@domain.com"
    				}
  				},

  					
  				errorPlacement: function(error, element) {
    					error.appendTo( element.parent().find(".error-wrapper") );
  				},

			});

})();
		

	
