<?php


//response generation function
  $response = "";
 
  //function to generate response
  function my_contact_form_generate_response($type, $message){
 
    global $response;
 
    if($type == "success") $response = "<div class='success'>{$message}</div>";
    else $response = "<div class='error'>{$message}</div>";
 
  }

  //response messages
$missing_content = "Por favor rellenar todos los campos";
$email_invalid   = "La dirección de correo es inválida";
$message_unsent  = "El mensaje no pudo ser enviado. Intente de nuevo.";
$message_sent    = "¡Gracias! Su mensaje ha sido enviado.";
 
//user posted variables
$name = $_POST['name'];
$email = $_POST['email'];
$message = $_POST['message'];
$city = $_POST['city'];
 
//php mailer variables
$to = get_option('admin_email');
$subject = "Someone sent a message from " . get_bloginfo('name');
$headers = 'From: ' . $email . "\r\n" .
  'Reply-To: ' . $email . "\r\n";

 if ($_POST['submitted']!=1){

	 my_contact_form_generate_response("error", $missing_content);
 
 }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {	
//validate email
	my_contact_form_generate_response("error", $email_invalid);

 }elseif (empty($name) || empty($message)) {
 //email is valid
 //validate presence of name and message	

	my_contact_form_generate_response("error", $missing_content);

 }else{
 	//send email
 	my_contact_form_generate_response("success", $message_sent); //message sent!
 }

 header('Location: ' . get_site_url() . "/?response=" . $response);
