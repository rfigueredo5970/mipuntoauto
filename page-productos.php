<?php 
	get_header();
?>

<?php /* Template Name: Productos */;?>

	<section class="content-productos">
<div id="productos" ></div>
	<div class="container">
	<h2>Productos</h2>

	<?php $productos_query = new WP_Query( array( 'category_name' => 'productos' ) ) ;?>

		<div class="row">
			<?php if ( $productos_query->have_posts() ) : while ( $productos_query->have_posts() ) : $productos_query->the_post(); 
					$post = get_post();
			?>

				<article class="col-xs-12 col-sm-6 col-md-4 center-block text-center">
			 	<h3><?php the_title(); ?></h3>
			 	<br>
				<?php
					if ( has_post_thumbnail() ) {
										
							the_post_thumbnail('list_articles_thumbs', array('class' => 'thumb img-responsive img-circle'));
					}
					else{
													
					echo '<img src="' . get_bloginfo( 'template_url' ) . '/img/lock200x200.png" alt="" class="thumb img-responsive img-circle">';
					}
				?>
			 	<p><?php the_content(); ?></p>
			 </article>

			 <?php endwhile; else: ?>		
				<h4>No se encontraron productos</h4>
			<?php endif; ?>
	<?php  wp_reset_query() ?>

		</div>		
	</div>
</section>

<!-- Footer -->
<?php
	get_footer();
?>